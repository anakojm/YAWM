# YAWM - Yet Another Wynncraft Modpack

just a collection of cool mod for wynncraft

list of mod used (31) :

- Ambience

- Ambient sound

- Auroragsi

- BetterFps

- BetterHUD

- Chunk pregenerator

- Chunk Animator

- Clumps

- Controlling

- Craft Studio

- Creative core

- Custom Main Menu

- Dynamic lights

- Dynamic Surrounding

- Foamfix

- Forgelin

- Fps reducer

- Optifine  (NOTE: for some reason the devs don't want ppl to give the jar directly, just download it and put it in mods/)

- OreLib

- Phosphor

- Random patches

- Real render

- Reauth

- Resource Loader

- Smooth Font

- Sound Filters

- Surge

- TexFix

- Vanilla Fix

- Version checker

- Wynntils
